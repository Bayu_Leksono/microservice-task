package id.co.sigma.minicore.minicore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinicoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinicoreApplication.class, args);
	}

}
