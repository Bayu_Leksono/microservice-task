package id.co.sigma.login.controller;


import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import id.co.sigma.login.request.LoginRequest;
import id.co.sigma.login.response.AuthTokenInfo;
import id.co.sigma.login.response.BaseResponse;

import java.util.Arrays;
import java.util.LinkedHashMap;

@Controller("loginController")
public class LoginController {



    public static final String AUTH_SERVER_URI = "http://localhost:8081/minicore/oauth/token";



    @RequestMapping(value="/customer/login**", method = {RequestMethod.POST})
    public @ResponseBody
    BaseResponse login(@RequestBody LoginRequest request){
        BaseResponse response = new BaseResponse();
        AuthTokenInfo tokenInfo = sendTokenRequest(request.getUsername(),request.getPassword());
        if(tokenInfo.getError()==null){
            response.setResponseCode("00");
            response.setResponseMessage("LOGIN SUCCESS");
            response.setAccessToken(tokenInfo.getAccess_token());
        }else{
            response.setResponseCode("01");
            response.setResponseMessage("LOGIN FAILED");
            response.setAccessToken(tokenInfo.getError());
        }
        return response;
    }

    private static HttpHeaders getHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

    private static HttpHeaders getHeadersWithClientCredentials(){
        String plainClientCredentials="sigma-client:sigma-secret";
        String base64ClientCredentials = new String(Base64.encodeBase64(plainClientCredentials.getBytes()));
        HttpHeaders headers = getHeaders();
        headers.add("Authorization", "Basic " + base64ClientCredentials);
        return headers;
    }

    private static AuthTokenInfo sendTokenRequest(String username, String password){

        String QPM_PASSWORD_GRANT = "?grant_type=password&username="+username+"&password="+password;

        RestTemplate restTemplate = new RestTemplate();
        AuthTokenInfo tokenInfo = null;
        HttpEntity<String> request = new HttpEntity<String>(getHeadersWithClientCredentials());
        try{
            ResponseEntity<Object> response = restTemplate.exchange(AUTH_SERVER_URI+QPM_PASSWORD_GRANT, HttpMethod.POST, request, Object.class);
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>)response.getBody();
            if(map!=null){
                tokenInfo = new AuthTokenInfo();
                tokenInfo.setAccess_token((String)map.get("access_token"));
                tokenInfo.setToken_type((String)map.get("token_type"));
                tokenInfo.setRefresh_token((String)map.get("refresh_token"));
                tokenInfo.setExpires_in((Integer)map.get("expires_in"));
                tokenInfo.setScope((String)map.get("scope"));
                System.out.println(tokenInfo);
            }else{
                System.out.println("No user exist----------");

            }
        }catch (HttpClientErrorException e){
                tokenInfo = new AuthTokenInfo();
                tokenInfo.setError(e.toString()+" Bad Credential");
        }
        return tokenInfo;
    }

}
