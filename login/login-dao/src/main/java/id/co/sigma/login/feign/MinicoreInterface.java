package id.co.sigma.login.feign;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.sigma.login.model.Customer;
import id.co.sigma.login.response.MessageResponse;


@FeignClient("minicore")
public interface MinicoreInterface {
	
	@RequestMapping(path = "/minicore/customer/get/all/{access_token}")
	public List<Customer> getAll(@PathParam(value = "access_token") String accessToken);
	
	@RequestMapping(path = "/minicore/customer/add")
	public MessageResponse add(@RequestBody Customer customer);
	
	@RequestMapping(value = "/minicore/webservice/listUser", method = { RequestMethod.GET })
	List listUser();
}
