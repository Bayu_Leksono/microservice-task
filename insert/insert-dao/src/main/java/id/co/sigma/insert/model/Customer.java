package id.co.sigma.insert.model;

import java.io.Serializable;
import java.util.Set;


public class Customer implements Serializable {
	
	private Long idCustomer;
	private String name;
	private String phoneNumber;
	private String address;
	private Set<Account> account;

	public Customer(Long idCustomer, String name, String phoneNumber, String address, Set<Account> account) {
		super();
		this.idCustomer = idCustomer;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.account = account;
	}

	public Customer() {
		super();
	}

	public Long getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Long idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Account> getAccount() {
		return account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}
	
}

