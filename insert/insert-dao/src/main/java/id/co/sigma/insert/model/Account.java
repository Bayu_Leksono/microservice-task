package id.co.sigma.insert.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;



public class Account implements Serializable {
	
	
	private Long idAccount;

	
	private String number;

	
	private BigDecimal balance;

	@JsonIgnore
	
	private Customer idCustomer;

	public Account(Long idAccount, String number, BigDecimal balance, Customer idCustomer) {
		super();
		this.idAccount = idAccount;
		this.number = number;
		this.balance = balance;
		this.idCustomer = idCustomer;
	}

	public Account() {
		super();
	}

	public Long getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(Long idAccount) {
		this.idAccount = idAccount;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Customer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Customer idCustomer) {
		this.idCustomer = idCustomer;
	}
	
}
