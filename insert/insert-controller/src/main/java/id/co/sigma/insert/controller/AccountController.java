package id.co.sigma.insert.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.insert.model.Account;
import id.co.sigma.insert.response.MessageResponse;
import id.co.sigma.insert.service.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService service;
	
	@RequestMapping(path = "/account/get/all")
	public List<Account> getAllAccounts() {
		return service.findAll();
	}
	
	@RequestMapping(path = "/account/add")
	public MessageResponse add(@RequestBody Account account) {
		return service.save(account);
	}
	
	@RequestMapping(path = "/account/update")
	public MessageResponse update(@RequestBody Account account) {
		return service.update(account);
	}
	
	@RequestMapping(path = "/account/delete/{id}")
	public List<Account> deleteById(@PathVariable("id") Long id) {
		service.deleteById(id);
		return getAllAccounts();
	}
	
	@RequestMapping(path = "account/inquiry/{id}/all")
	public List<Account> inquiryAll(@PathVariable("id") Long id){
		return service.inquiryAll(id);
		
	}
}
