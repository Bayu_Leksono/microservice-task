package id.co.sigma.insert.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.insert.feign.MinicoreInterface;
import id.co.sigma.insert.model.Customer;
import id.co.sigma.insert.response.MessageResponse;

@Service
public class CustomerService {

	@Autowired
	private MinicoreInterface minicoreInterface;
	
	public List<Customer> findAll() {
		return minicoreInterface.getAll();
	}

	public MessageResponse save(Customer customer) {
		return minicoreInterface.add(customer);
	}
	
	public MessageResponse update(Customer customer) {
		return minicoreInterface.updateCostumer(customer);
	}
	
	public List<Customer> deleteByIdcus(Long id) {
		return minicoreInterface.deleteById(id);
	}
	
	public Customer findByIdCustomer(Long id) {
		return minicoreInterface.getByIdCustomer(id);
	}

	
}
	
