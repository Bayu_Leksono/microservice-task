package id.co.sigma.insert.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import id.co.sigma.insert.feign.MinicoreInterface;

public class UserService {

	@Autowired
	private MinicoreInterface minicoreInterface;
	
	public List listUser() {
		return minicoreInterface.listUser();
	}
}
