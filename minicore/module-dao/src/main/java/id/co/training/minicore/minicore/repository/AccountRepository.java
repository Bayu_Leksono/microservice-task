package id.co.training.minicore.minicore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.training.minicore.minicore.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	public Account findByNumber(String number);
}
