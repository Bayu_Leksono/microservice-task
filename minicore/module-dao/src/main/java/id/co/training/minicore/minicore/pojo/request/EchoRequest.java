package id.co.training.minicore.minicore.pojo.request;

public class EchoRequest {
    private String requestCode;

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }
}
