package id.co.training.minicore.minicore.repository;

import id.co.training.minicore.minicore.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Long> {

    Role findByAuthority(String roleName);

}
