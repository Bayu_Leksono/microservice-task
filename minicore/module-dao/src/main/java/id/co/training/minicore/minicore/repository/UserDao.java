package id.co.training.minicore.minicore.repository;

import id.co.training.minicore.minicore.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<Users, Long> {
    Users findByUsernameAndPassword(String username, String password);

    Users findByUsername(String username);
}
