package id.co.training.minicore.minicore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import id.co.training.minicore.minicore.model.Customer;

@Service
public interface CustomerRepository extends JpaRepository<Customer, Long>{

	

}
