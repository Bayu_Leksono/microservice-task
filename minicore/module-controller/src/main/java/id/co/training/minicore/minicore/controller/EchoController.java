package id.co.training.minicore.minicore.controller;


import org.springframework.web.bind.annotation.*;

import id.co.training.minicore.minicore.pojo.request.EchoRequest;
import id.co.training.minicore.minicore.pojo.response.BaseResponse;

import java.io.IOException;


@RestController
public class EchoController {

	public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";

	@RequestMapping(value = TERMINAL_ECHO_PATH, method = RequestMethod.POST)
	public @ResponseBody
    BaseResponse echo(@RequestBody EchoRequest request) throws IOException {
		BaseResponse response = new BaseResponse();
		if (request.getRequestCode().equals("ECHO")) {
			response.setResponseCode("00");
			response.setResponseMessage("SUCCESS");
		} else {
			response.setResponseCode("01");
			response.setResponseMessage("FAILED");
		}
		return response;
	}
}