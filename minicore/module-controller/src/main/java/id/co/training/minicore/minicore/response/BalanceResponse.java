package id.co.training.minicore.minicore.response;

public class BalanceResponse {

	String balance;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	
}
