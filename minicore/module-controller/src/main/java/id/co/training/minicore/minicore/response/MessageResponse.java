package id.co.training.minicore.minicore.response;

public class MessageResponse {

	String message;

	
	public MessageResponse(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
