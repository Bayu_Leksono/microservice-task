package id.co.training.minicore;

import java.util.regex.Pattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;

@SpringBootApplication
@EnableAutoConfiguration
@ConfigurationProperties
@EnableDiscoveryClient
@EnableCaching
public class MinicoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinicoreApplication.class, args);
		
//		Pattern BCRYPT_PATTERN = Pattern
//				.compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");
//		
//		String sigma = new BCryptPasswordEncoder().encode("11duadua");
//		System.out.println(sigma);
//		
//		BCryptPasswordEncoder ok = new BCryptPasswordEncoder();
//		
//		if (!BCRYPT_PATTERN.matcher(sigma).matches()) {
//			System.out.println("gagaaaaall lbroooooo ");
//		} 
//		
//		System.out.println(ok.matches("sigma-secret", sigma));
		

	}

}
