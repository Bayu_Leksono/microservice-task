package id.co.sigma.transaction.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import id.co.sigma.transaction.feign.MinicoreInterface;
import id.co.sigma.transaction.model.Customer;
import id.co.sigma.transaction.request.DepositRequest;
import id.co.sigma.transaction.request.TransferRequest;
import id.co.sigma.transaction.request.WithdrawalRequest;
import id.co.sigma.transaction.response.DepositResponse;
import id.co.sigma.transaction.response.MessageResponse;
import id.co.sigma.transaction.response.WithdrawalResponse;

@Service
public class CustomerService {

//	@Autowired
//	private CustomerRepository repository;

	@Autowired
	private MinicoreInterface minicoreInterface;
	
	public DepositResponse deposit(@RequestBody DepositRequest request) {
		return minicoreInterface.deposit(request);
	}

	public ResponseEntity<Object> transfer(@RequestBody TransferRequest request){
		return minicoreInterface.transfer(request);
	}
	
	public WithdrawalResponse withdrawal(@RequestBody WithdrawalRequest request) {
		return minicoreInterface.withdrawal(request);
	}
	
	

}