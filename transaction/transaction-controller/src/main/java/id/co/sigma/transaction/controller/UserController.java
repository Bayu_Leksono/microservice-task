package id.co.sigma.transaction.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.authentication.UserServiceBeanDefinitionParser;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import id.co.sigma.transaction.service.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.management.relation.Role;

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    
    @RequestMapping(value ="/webservice/listUser", method = (RequestMethod.GET))
    public @ResponseBody List listUser() {
    	List userList = userService.listUser();
    	return userList;
    }
    

}
